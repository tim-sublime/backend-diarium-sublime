const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const app = express();
const port = 5050;
const router = require("./src/router.js");
const multer = require("multer");
const diskStorage = require("./src/config/storage");
require("./src/config/mongoo");

// Use middleware
// Enable CORS
app.use(cors());

// Enable Body Parser
// for parsing application/json
app.use(bodyParser.json());

//for parsing application/xwww-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Init router
app.use("/api/v1", router);

// Example upload file
// app.post(
//   "/upload",
//   multer({ storage: diskStorage("images") }).single("avatar"),
//   function (req, res) {
//     console.log(req.file);
//     const file_url =
//       req.protocol +
//       "://" +
//       req.get("host") +
//       "/" +
//       req.file.destination +
//       "/" +
//       req.file.filename;
//     const file = req.file.path;
//     if (!file) {
//       res.status(400).send({
//         status: false,
//         data: "No File is selected.",
//       });
//     }
//     res.send(file);
//   }
// );

// Access static folder
app.use("/uploads", express.static(__dirname + "/uploads/"));

// Serve local
app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
