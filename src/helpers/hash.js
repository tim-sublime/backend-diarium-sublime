const bcrypt = require("bcrypt");

exports.hash = async (password) => {
  const hash = await bcrypt.hash(password, 10);
  return hash;
};

exports.compare = async (password, hash) => {
  let compare = await bcrypt.compare(password, hash);
  return compare;
};
