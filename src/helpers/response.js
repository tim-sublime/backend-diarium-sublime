function errorResponse(res, messages) {
  res.status(500).json(messages);
}

function successResponse(res, messages) {
  res.status(200).json(messages);
}

function entityResponse(res, messages) {
  res.status(422).json(messages);
}

function customResponse(res, code, messages) {
  res.status(code).json(messages);
}

module.exports = {
  errorResponse,
  successResponse,
  entityResponse,
  customResponse,
};
