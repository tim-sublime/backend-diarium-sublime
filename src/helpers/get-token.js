const response = require("./response");

function getToken(req, res) {
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader != "undefined") {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1];
    return bearerToken;
  } else {
    return response.customResponse(res, 403, {
      status: "error",
      message: "Unauthorized access token",
    });
  }
}

module.exports = { getToken };
