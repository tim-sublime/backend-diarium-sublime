const multer = require("multer");
const path = require("path");

const diskStorage = (foldername) =>
  multer.diskStorage({
    destination: "uploads/" + foldername,
    filename: function (req, file, cb) {
      cb(
        null,
        file.fieldname + "-" + Date.now() + path.extname(file.originalname)
      );
    },
  });

module.exports = diskStorage;
