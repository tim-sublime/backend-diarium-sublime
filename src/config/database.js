const { Sequelize } = require("sequelize");

let type = "mysql";
let port = 3306;
let host = "localhost";
let database = "test";
let username = "root";
let password = "";

const sequelize = new Sequelize(database, username, password, {
  host: host,
  port: port,
  dialect: type,
});

sequelize
  .authenticate()
  .then((res) => {
    console.log("Success connecting to database.");
  })
  .catch((err) => {
    console.log("Error when connecting to database :", err);
    process.exit(1);
  });

module.exports = sequelize;
