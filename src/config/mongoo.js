const mongoose = require("mongoose");

let db = "diarium";
let host = "10.128.16.27:27017";

//Set up default mongoose connection
mongoose.connect(`mongodb://${host}/${db}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

//Get the default connection
const connection = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
connection.once("open", function () {
  console.log("MongoDB connection successfull");
});

connection.once("error", function () {
  console.log("ongoDB connection error");
});

module.exports = connection;
