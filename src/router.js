const router = require("express").Router();
const userController = require("./controllers/users/users.controller");
const absentController = require("./controllers/absent/absent.controller");
const activityController = require("./controllers/activity/activity.controller");

router.use("/", userController);
router.use("/absent", absentController);
router.use("/activity", activityController);

module.exports = router;
