const mongoose = require("mongoose");
const { Schema } = mongoose;

const userSchema = new Schema(
  {
    nik: {
      type: "String",
      minlength: 5,
      unique: true,
    },
    password: {
      type: "String",
      minlength: 8,
    },
    firstname: {
      type: "String",
    },
    lastname: {
      type: "String",
    },
    email: {
      type: "String",
      unique: true,
    },
    phone_number: {
      type: "String",
      unique: true,
    },
    avatar: {
      type: "String",
      unique: true,
    },
  },
  { autoCreate: true, versionKey: false }
);

const userModel = mongoose.model("users", userSchema);

module.exports = userModel;
