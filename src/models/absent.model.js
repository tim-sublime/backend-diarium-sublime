const mongoose = require("mongoose");
const { Schema } = mongoose;

const absentSchema = new Schema(
  {
    user_id: {
      type: "String",
    },
    check_date: {
      type: "String",
    },
    checkin_time: {
      type: "String",
    },
    checkin_location: {
      type: "String",
      enum: ["kantor", "rumah", "satelit"],
    },
    checkin_condition: {
      type: "String",
      enum: ["sehat", "kurang_fit", "sakit"],
    },
    checkout_time: {
      type: "String",
      default: "null",
    },
    checkout_location: {
      type: "String",
      enum: ["null", "kantor", "rumah", "satelit"],
      default: "null",
    },
    checkout_condition: {
      type: "String",
      enum: ["null", "sehat", "kurang_fit", "sakit"],
      default: "null",
    },
  },
  { autoCreate: true, versionKey: false }
);

const absentModel = mongoose.model("checkinout", absentSchema);

module.exports = absentModel;
