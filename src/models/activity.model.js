const mongoose = require("mongoose");
const { Schema } = mongoose;

const activityShema = new Schema(
  {
    user_id: {
      type: "String",
    },
    activity_title: {
      type: "String",
    },
    priority_rate: {
      type: "String",
    },
    create_date: {
      type: "Date",
    },
    start_date: {
      type: "String",
    },
    end_date: {
      type: "String",
    },
    progress: {
      type: "Number",
    },
    tag: {
      type: "String",
    },
  },
  { autoCreate: true, versionKey: false }
);

const activityModel = mongoose.model("activities", activityShema);

module.exports = activityModel;
