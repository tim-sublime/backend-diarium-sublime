const { validationResult } = require("express-validator");
const response = require("../helpers/response");
const jwt = require("jsonwebtoken");
const token = require("../helpers/get-token");
const absentModel = require("../models/absent.model");
const moment = require("moment");

exports.checkin = async (req, res) => {
  try {
    // Check validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return response.entityResponse(res, {
        status: "error",
        message: errors.mapped(),
      });
    }

    let bearerToken = await token.getToken(req, res);
    let user = jwt.decode(bearerToken);
    let id = user.user._id;
    let date = moment().format("YYYY-M-D");

    let find = await absentModel
      .findOne({ user_id: id, check_date: date })
      .exec();

    if (find != null) {
      if (find.checkin_time != null) {
        return response.entityResponse(res, {
          status: "error",
          message: "Check in time is already exists",
        });
      }
    }

    const absent = new absentModel({
      user_id: id,
      check_date: date,
      checkin_time: req.body.checkin_waktu,
      checkin_location: req.body.checkin_lokasi,
      checkin_condition: req.body.checkin_kondisi,
    });

    absent.save(function (err, doc) {
      if (err) {
        throw new Error(err);
      } else {
        console.log("Check in registered");
      }
    });

    console.log(absent);

    return response.successResponse(res, {
      status: "success",
      message: "Check in success",
    });
  } catch (error) {
    return response.errorResponse(res, {
      status: "error",
      message: error,
    });
  }
};

exports.checkout = async (req, res) => {
  try {
    // Check validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return response.entityResponse(res, {
        status: "error",
        message: errors.mapped(),
      });
    }

    let bearerToken = await token.getToken(req, res);
    let user = jwt.decode(bearerToken);
    let id = user.user._id;
    let date = moment().format("YYYY-M-D");

    let find = await absentModel
      .findOne({ user_id: id, check_date: date })
      .exec();

    console.log(find);

    if (find != null) {
      if (find.checkout_time != "null") {
        return response.entityResponse(res, {
          status: "error",
          message: "Check out time already exists",
        });
      }
    }

    if (find == null) {
      return response.entityResponse(res, {
        status: "error",
        message: "Check in time is empty",
      });
    }

    let absent = await absentModel.findOneAndUpdate(
      { user_id: id },
      {
        checkout_time: req.body.checkout_waktu,
        checkout_location: req.body.checkout_lokasi,
        checkout_condition: req.body.checkout_kondisi,
      },
      { upsert: true },
      function (err, doc) {
        if (err) {
          throw new Error(err);
        } else {
          console.log("Check out registered");
        }
      }
    );

    console.log(absent);

    return response.successResponse(res, {
      status: "success",
      message: "Check out success",
    });
  } catch (error) {
    return response.errorResponse(res, {
      status: "error",
      message: error,
    });
  }
};
