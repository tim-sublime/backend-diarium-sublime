const userModel = require("../models/users.model");
const { validationResult } = require("express-validator");
const response = require("../helpers/response");
const crypt = require("../helpers/hash");
const jwt = require("jsonwebtoken");
const key = require("../config/jwt");
const token = require("../helpers/get-token");

exports.login = async function (req, res) {
  try {
    // Check validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return response.entityResponse(res, {
        status: "error",
        message: errors.mapped(),
      });
    }

    let user = await userModel.findOne({ nik: req.body.nik }).exec();
    if (!user) {
      return response.entityResponse(res, {
        status: "error",
        message: "NIK not found",
      });
    }
    console.log(user);

    if (!(await crypt.compare(req.body.password, user.password))) {
      return response.entityResponse(res, {
        status: "error",
        message: "Wrong password",
      });
    }

    const token = jwt.sign({ user }, key, {
      expiresIn: "30 days",
    });

    return response.successResponse(res, {
      status: "success",
      message: "Login success",
      access: {
        token: token,
        iat: 1607379301,
        exp: 1609971301,
      },
    });
  } catch (error) {
    return response.errorResponse(res, {
      status: "error",
      message: error,
    });
  }
};

exports.register = async function (req, res) {
  try {
    // Check validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return response.entityResponse(res, {
        status: "error",
        message: errors.mapped(),
      });
    }

    // Check if nik already exists
    let find = await userModel
      .findOne({
        nik: req.body.nik,
      })
      .exec();
    console.log(find);

    if (find != null) {
      if (find.nik === req.body.nik) {
        return response.entityResponse(res, {
          status: "error",
          message: "NIK already exists",
        });
      }
    }

    // Check if email already exists
    find = await userModel
      .findOne({
        email: req.body.email,
      })
      .exec();
    console.log(find);

    if (find != null) {
      if (find.email === req.body.email) {
        return response.entityResponse(res, {
          status: "error",
          message: "Email already exists",
        });
      }
    }

    // Check if phone number already exists
    find = await userModel
      .findOne({
        phone_number: req.body.phone_number,
      })
      .exec();
    console.log(find);

    if (find != null) {
      if (find.phone_number === req.body.phone_number) {
        return response.entityResponse(res, {
          status: "error",
          message: "Phone number already exists",
        });
      }
    }

    if (req.body.password != req.body.password_confirmation) {
      return response.entityResponse(res, {
        status: "error",
        message: "Confirmation password is not same",
      });
    }

    const hash = await crypt.hash(req.body.password);

    const user = new userModel({
      nik: req.body.nik,
      password: hash,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      phone_number: req.body.phone_number,
      avatar:
        req.protocol +
        "://" +
        req.get("host") +
        "/" +
        "uploads/images/user.png",
    });

    user.save(function (err, doc) {
      if (err) {
        throw new Error(err);
      } else {
        console.log("User registered");
      }
    });

    console.log(user);

    return response.successResponse(res, {
      status: "success",
      message: "Register success",
    });
  } catch (error) {
    return response.errorResponse(res, {
      status: "error",
      message: error,
    });
  }
};

exports.checktoken = async (req, res) => {
  try {
    let bearerToken = await token.getToken(req, res);

    let verify = jwt.verify(bearerToken, key);

    if (!verify) {
      return response.customResponse(res, 401, "Expired");
    }

    return response.successResponse(res, {
      status: "success",
      message: "Check token success",
      data: verify,
    });
  } catch (error) {
    return response.errorResponse(res, {
      status: "error",
      message: error,
    });
  }
};
