const { validationResult } = require("express-validator");
const response = require("../helpers/response");
const jwt = require("jsonwebtoken");
const token = require("../helpers/get-token");
const activityModel = require("../models/activity.model");
const moment = require("moment");

exports.show = async (req, res) => {
  try {
    let bearerToken = await token.getToken(req, res);
    let user = jwt.decode(bearerToken);
    let id = user.user._id;

    let find = await activityModel.find({ user_id: id }).exec();

    return response.successResponse(res, {
      status: "success",
      message: "Success get all activity",
      data: find,
    });
  } catch (error) {
    return response.errorResponse(res, {
      status: "error",
      message: error,
    });
  }
};

exports.get = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return response.entityResponse(res, {
        status: "error",
        message: errors.mapped(),
      });
    }

    let bearerToken = await token.getToken(req, res);
    let user = jwt.decode(bearerToken);
    let id = user.user._id;

    let find = await activityModel
      .find({ _id: req.query.id, user_id: id })
      .exec();

    console.log(find);

    if (find == null) {
      return response.customResponse(res, 404, {
        status: "error",
        message: "Activity not found",
      });
    }

    return response.successResponse(res, {
      status: "success",
      message: "Success get activity",
      data: find,
    });
  } catch (error) {
    return response.errorResponse(res, {
      status: "error",
      message: error,
    });
  }
};

exports.create = async (req, res) => {
  try {
    // Check validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return response.entityResponse(res, {
        status: "error",
        message: errors.mapped(),
      });
    }

    let bearerToken = await token.getToken(req, res);
    let user = jwt.decode(bearerToken);
    let id = user.user._id;

    const activity = new activityModel({
      user_id: id,
      activity_title: req.body.activity_title,
      priority_rate: req.body.priority_rate,
      create_date: new Date(),
      start_date: moment(req.body.start_date).format("YYYY-M-D"),
      end_date: moment(req.body.end_date).format("YYYY-M-D"),
      progress: req.body.progress,
      tag: req.body.tag,
    });

    activity.save(function (err, doc) {
      if (err) {
        throw new Error(err);
      } else {
        console.log("Activity is registered");
      }
    });

    console.log(activity);

    return response.successResponse(res, {
      status: "success",
      message: "Register activity success",
    });
  } catch (error) {
    return response.errorResponse(res, {
      status: "error",
      message: error,
    });
  }
};

exports.edit = async (req, res) => {
  try {
    // Check validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return response.entityResponse(res, {
        status: "error",
        message: errors.mapped(),
      });
    }

    let find = await activityModel.findById(req.body.id).exec();

    console.log(find);

    if (find == null) {
      return response.customResponse(res, 404, {
        status: "error",
        message: "Activity not found",
      });
    }

    let activity = await activityModel.findOneAndUpdate(
      { _id: req.body.id },
      {
        activity_title: req.body.activity_title,
        priority_rate: req.body.priority_rate,
        start_date: moment(req.body.start_date).format("YYYY-M-D"),
        end_date: moment(req.body.end_date).format("YYYY-M-D"),
        progress: req.body.progress,
        tag: req.body.tag,
      },
      function (err, doc) {
        if (err) {
          throw new Error(err);
        } else {
          console.log("Activity is updated");
        }
      }
    );

    console.log(activity);

    return response.successResponse(res, {
      status: "success",
      message: "Update activity success",
    });
  } catch (error) {
    return response.errorResponse(res, {
      status: "error",
      message: error,
    });
  }
};

exports.delete = async (req, res) => {
  try {
    // Check validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return response.entityResponse(res, {
        status: "error",
        message: errors.mapped(),
      });
    }

    let find = await activityModel.findById(req.body.id).exec();

    console.log(find);

    if (find == null) {
      return response.customResponse(res, 404, {
        status: "error",
        message: "Activity not found",
      });
    }

    let activity = await activityModel.findByIdAndDelete(
      { _id: req.body.id },
      function (err, doc) {
        if (err) {
          throw new Error(err);
        } else {
          console.log("Activity is deleted");
        }
      }
    );

    console.log(activity);

    return response.successResponse(res, {
      status: "success",
      message: "Delete activity success",
    });
  } catch (error) {
    return response.errorResponse(res, {
      status: "error",
      message: error,
    });
  }
};
