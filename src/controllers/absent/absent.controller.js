const router = require("express").Router();
const key = require("../../config/jwt");
const { withJWTAuthMiddleware } = require("express-kun");
const absent_service = require("../../services/absent.service");
const absent_validation = require("./absent.validation");

const protectedRouter = withJWTAuthMiddleware(router, key);

protectedRouter.post(
  "/check-in",
  absent_validation.validate("checkin"),
  absent_service.checkin
);

protectedRouter.post(
  "/check-out",
  absent_validation.validate("checkout"),
  absent_service.checkout
);

module.exports = router;
