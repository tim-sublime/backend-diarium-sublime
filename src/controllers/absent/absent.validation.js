const { body } = require("express-validator");

exports.validate = (method) => {
  switch (method) {
    case "checkin": {
      return [
        body("checkin_waktu").isString(),
        body("checkin_lokasi").isIn(["kantor", "rumah", "satelit"]),
        body("checkin_kondisi").isIn(["sehat", "kurang_fit", "sakit"]),
      ];
    }

    case "checkout": {
      return [
        body("checkout_waktu").isString(),
        body("checkout_lokasi").isIn(["kantor", "rumah", "satelit"]),
        body("checkout_kondisi").isIn(["sehat", "kurang_fit", "sakit"]),
      ];
    }
  }
};
