const router = require("express").Router();
const user_service = require("../../services/users.service");
const user_validation = require("../../controllers/users/users.validation");
const key = require("../../config/jwt");
const { withJWTAuthMiddleware } = require("express-kun");

const protectedRouter = withJWTAuthMiddleware(router, key);

router.post("/login", user_validation.validate("login"), user_service.login);
router.post(
  "/register",
  user_validation.validate("register"),
  user_service.register
);
protectedRouter.get("/check-token", user_service.checktoken);

module.exports = router;
