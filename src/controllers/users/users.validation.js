const { body } = require("express-validator");

exports.validate = (method) => {
  switch (method) {
    case "login": {
      return [
        body("nik").isNumeric(),
        body("password").isString().isLength({
          min: 8,
        }),
      ];
    }

    case "register": {
      return [
        body("nik").isNumeric(),
        body("password").isString().isLength({ min: 8 }),
        body("password_confirmation").isString().isLength({ min: 8 }),
        body("firstname").isString(),
        body("lastname").isString(),
        body("email").isEmail(),
        body("phone_number").isString().isLength({ min: 12 }),
      ];
    }
  }
};
