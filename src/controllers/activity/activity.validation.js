const { body, query } = require("express-validator");

exports.validate = (method) => {
  switch (method) {
    case "get": {
      return [query("id").isString()];
    }

    case "create": {
      return [
        body("activity_title").isString(),
        body("priority_rate").isString(),
        body("start_date").isString(),
        body("end_date").isString(),
        body("progress").isNumeric(),
        body("tag").isString(),
      ];
    }

    case "edit": {
      return [
        body("id").isString(),
        body("activity_title").isString(),
        body("priority_rate").isString(),
        body("start_date").isString(),
        body("end_date").isString(),
        body("progress").isNumeric(),
        body("tag").isString(),
      ];
    }

    case "delete": {
      return [body("id").isString()];
    }
  }
};
