const router = require("express").Router();
const key = require("../../config/jwt");
const { withJWTAuthMiddleware } = require("express-kun");
const activity_service = require("../../services/activity.service");
const activity_validation = require("./activity.validation");

const protectedRouter = withJWTAuthMiddleware(router, key);

protectedRouter.get(
  "/",
  activity_service.show
);

protectedRouter.get(
  "/find",
  activity_validation.validate("get"),
  activity_service.get
);

protectedRouter.post(
  "/",
  activity_validation.validate("create"),
  activity_service.create
);

protectedRouter.put(
  "/",
  activity_validation.validate("edit"),
  activity_service.edit
);

protectedRouter.delete(
  "/",
  activity_validation.validate("delete"),
  activity_service.delete
);

module.exports = router;
